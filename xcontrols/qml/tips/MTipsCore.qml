import QtQuick 2.0

Item {
    id: coreRoot;
    
    property alias text: textItem.text;
    property alias icon: iconImg.source;
    
    function init(jsObj){
        if( jsObj.code === -1 ){//暂无内容
            icon = "qrc:/image/empty.png";
            text = qsTr("暂无内容");
        }
        
        if( jsObj.message && jsObj.message.length > 0 )
            text = jsObj.message;
    }
    
    Column{
        id: column;
        width: parent.width;
        anchors.verticalCenter: parent.verticalCenter;
        anchors.verticalCenterOffset: -height*0.66;
        spacing: 10;
        Image {
            id: iconImg;
            //source: "qrc:/image/empty.png";
            width: 100;
            height: sourceSize.height/sourceSize.width*width;
            anchors.horizontalCenter: parent.horizontalCenter;
        }
        Text {
            id: textItem;
            //text: qsTr("暂无内容")
            font.pointSize: 10;
            anchors.horizontalCenter: parent.horizontalCenter;
        }
    }
}

