﻿import QtQuick 2.0
import QtQuick.Layouts 1.0
import "qrc:/xcqml/view/"
import "qrc:/MOpenJs.js" as OpenJs

Item {
    id: testRoot;
    property int num: 0;
    Component.onCompleted: init();
    
    function init(){ 
        var menus = [qsTr("新鲜蔬菜"),qsTr("时令水果"),qsTr("肉禽蛋"),qsTr("海鲜水产"),qsTr("米面粮油")];
        var childs = [qsTr("有机蔬菜;叶菜类;根茎类;香辛姜蒜;菌菇类;豆芽/芽苗")
                      ,qsTr("热带水果;柑橘橙柚;蓝莓/莓类;苹果梨蕉")
                      ,qsTr("")
                      ,qsTr("鲜活水产;鱼;虾/虾仁")
                      ,qsTr("米类;食用油;面粉面条")];
        
        for(var i=0;i<menus.length;i++){
            leftView.model.append({"title": menus[i],"childs": childs[i]});
        }
        leftView.currentIndex = 0;
    }
    
    function loadDatas(){
        var len =  num<0 ? view.limit : view.limit/2;
        num++;
        if( (parseInt(Math.random()*10)%2) == 0 )
            len = 0;
        var cD = new Date();
        for(var i=0;i<len;i++){
            datasModel.append({"text": leftView.curText()+"-"+topView.curText()});
        }
        view.setDatas(datasModel,len,{"code": (len>0?0:-1),"message": qsTr("")});
    }
    
    function changeCategory(isNext){
        if( isNext )
        {
            if(  topView.currentIndex >=0 && (topView.currentIndex+1) < topView.count )
                topView.currentIndex += 1;
            else if( (leftView.currentIndex+1) < leftView.count )
                leftView.currentIndex += 1;
            else
                leftView.currentIndex = 0;
        }
        else{
            if( topView.currentIndex > 0 )
                topView.currentIndex -= 1;
            else
            {
                leftView.isPrev = true;
                if( leftView.currentIndex > 0 )
                    leftView.currentIndex -= 1;
                else
                    leftView.currentIndex = leftView.count-1;
            }
        }
    }
    
    Timer{
        id: loadTimer;
        interval: 600;
        onTriggered: {
            if( !view.model ){
                num = 0;
                datasModel.clear();
            }
            loadDatas();
        }
    }
    
    ListModel{
        id: datasModel;
        
    }
    
    RowLayout{
        anchors.fill: parent;
        spacing: 0;
        
        ListView{
            id: leftView;
            property bool isPrev: false;
            Layout.preferredWidth: 80;
            Layout.fillHeight: true;
            currentIndex: -1;
            model: ListModel{}
            
            onCurrentIndexChanged: {
                if( currentIndex < 0 )
                    return;
                var cs = model.get(currentIndex).childs;
                
                topView.model.clear();
                topView.currentIndex = -1;
                if( cs.length > 0 ){
                    cs = cs.split(";");
                    for(var i=0;i<cs.length;i++)
                        topView.model.append({"title": cs[i]});
                }
                
                if( topView.count <=0 )
                    view.startLoading(true);
                else if( leftView.isPrev )
                    topView.currentIndex = (topView.count-1);
                else
                    topView.currentIndex = 0;
                leftView.isPrev = false;
            }
            
            function curText(){
                if( currentIndex >= 0 && currentIndex < count )
                    return model.get(currentIndex).title;
                return "";
            }
            
            delegate: Rectangle{
                width: leftView.width;
                height: 40;
                color: (index===leftView.currentIndex) ? "#e5e5e5" : "white";
                MouseArea{
                    anchors.fill: parent;
                    onClicked: leftView.currentIndex=index;
                }
                
                Text {
                    text: model.title;
                    font.pointSize: 11;
                    anchors.centerIn: parent;
                }
                Rectangle{
                    width: parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#f0f0f0";
                }
            }
        }
        
        Rectangle{
            Layout.fillHeight: true;
            Layout.preferredWidth: 1;
            color: "#f0f0f0";
        }
        
        ColumnLayout{
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            spacing: 0;
            ListView{
                id: topView;
                Layout.fillWidth: true;
                Layout.preferredHeight: 30;
                orientation: ListView.Horizontal;
                visible: count>0;
                spacing: 4;
                model: ListModel{}
                currentIndex: -1;
                header: Item{
                    width: 10;
                }
                
                footer: Item{
                    width: 10;
                }
                
                onCurrentIndexChanged: view.startLoading(true);
                
                function curText(){
                    if( currentIndex >=0 && currentIndex<count )
                        return model.get(currentIndex).title;
                    return "";
                }
                
                delegate: MouseArea{
                    height: 30
                    width: cMenuTextItem.contentWidth + 15;
                    onClicked: topView.currentIndex = index;
                    
                    Rectangle{
                        height: parent.height*0.8;
                        width: cMenuTextItem.parent.width;
                        radius: 2;
                        color: (parent.ListView.isCurrentItem ? "#e3fcdf" : "#f2f2f2");
                        anchors.centerIn: parent;
                    }
                    
                    Text {
                        id: cMenuTextItem;
                        text: model.title;
                        font.pointSize: 10;
                        anchors.centerIn: parent;
                        color: (parent.ListView.isCurrentItem ? "#3d8345" : "#535458");
                    }
                }
            }
            
            Rectangle{
                Layout.fillWidth: true;
                Layout.preferredHeight: 1;
                color: "#f0f0f0";
            }
            
            MListView_infiniteSliding{
                id: view;
                Layout.fillWidth: true;
                Layout.fillHeight: true;
                
                onLoadMore: {
                    loadTimer.start();
                }
                
                onSwitchCategory:{
                    changeCategory(isNex);
                }
                
                delegate: Item{
                    width: view.width;
                    height: 80;
                    Text {
                        text: qsTr("%1: %2").arg(index+1).arg(model.text);
                        font.pointSize: 12;
                        anchors.verticalCenter: parent.verticalCenter;
                        anchors.left: parent.left;
                        anchors.leftMargin: 20;
                    }
                    
                    Rectangle{
                        width: parent.width;
                        height: 1;
                        anchors.bottom: parent.bottom;
                        color: "#f5f5f5"
                    }
                }
            }
        }
    }
    
}

